# Copy the files from the dist to the container
FROM nginx:stable
COPY dist/writing-client /usr/share/nginx/html

# Build the image
# docker build -t moronya/mwandishi-client .

# Run the container
# docker run -p 80:80 moronya/mwandishi-client

# Copy the config
COPY nginx.conf /etc/nginx/nginx.conf
COPY ssl/mwandishi.crt /etc/nginx/certs/
COPY ssl/private.key /etc/nginx/certs/