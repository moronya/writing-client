import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './entities/home/home.component';
import { LoginComponent } from './auth/login/login.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { OrdersComponent } from './orders/orders.component';
import { OrganizationDetailComponent } from './organization-detail/organization-detail.component';
import { OrganizationsComponent } from './organizations/organizations.component';
import { RegisterComponent } from './register/register.component';
import { ApiKeyComponent } from './entities/api-key/api-key.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'manage/organizations', pathMatch: 'full'
  },
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'register',
    component:RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'manage/organizations',
    component: OrganizationsComponent,
    data: {
      title: 'organizations',
      breadcrumb: [
        {
          label: 'manage',
          url: ''
        },
        {
          label: 'organizations',
          url: '/manage/organizations'
        }
      ]
    }
  },
  {
    path: 'manage/organizations/:id',
    component: OrganizationDetailComponent,
    data: {
      breadcrumb: [
        {
          label: 'manage',
          url: ''
        },
        {
          label: 'organizations',
          url: '/manage/organizations'
        },
        {
          label: '{{id}}',
          url: '/manage/organizations/:id'
        }
      ]
    }
  },
  {
    path: 'manage/organizations/:id/:orgid',
    component: InvoiceComponent,
    data: {
      breadcrumb: [
        {
          label: 'manage',
          url: ''
        },
        {
          label: 'organizations',
          url: '/manage/organizations'
        },
        {
          label: '{{id}}',
          url: '/manage/organizations/:id'
        },
        {
          label: '{{orgid}}',
          url: '/manage/organizations/:id/:orgid'
        }
      ]
    }
  },
  {
    path: 'manage/organizations/:id/manage/orders',
    component: OrdersComponent,
    data: {
      breadcrumb: [
        {
          label: 'manage',
          url: ''
        },
        {
          label: 'organizations',
          url: '/manage/organizations'
        },
        {
          label: '{{id}}',
          url: '/manage/organizations/:id'
        },
        {
          label: 'orders',
          url: '/manage/organizations/:id/manage/orders'
        }
      ]
    }
  },
  {
    path: 'manage/organizations/:id/manage/apikey',
    component: ApiKeyComponent,
    data: {
      breadcrumb: [
        {
          label: 'manage',
          url: ''
        },
        {
          label: 'organizations',
          url: '/manage/organizations'
        },
        {
          label: '{{id}}',
          url: '/manage/organizations/:id'
        },
        {
          label: 'api-key',
          url: '/manage/organizations/:id/manage/api-key'
        }
      ]
    }
  }



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
