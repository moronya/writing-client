import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (authService.getToken() != null) {
          this.isLoggedIn = true
        } else {
          this.isLoggedIn = false
        }

      }
    }
    )
  }
  isLoggedIn!: boolean;


  ngOnInit(): void {

  }

  // logout functionality
  logOut() {
    this.isLoggedIn = false;
    this.authService.clearToken();
    this.router.navigate(['/login']);
  }

}
