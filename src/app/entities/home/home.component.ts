import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private auth: AuthService,
    private router: Router
  ) 
  {    
  }
  isLoggedIn!: boolean;

 

  ngOnInit(): void {
  this.checkLogin();
  }

  checkLogin() {
    if(this.auth.getToken ()  != null){
      this.isLoggedIn = true
    }
    else{
      this.isLoggedIn = false
    }
    return this.isLoggedIn
  }

}
