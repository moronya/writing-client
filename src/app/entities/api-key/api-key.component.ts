import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { OrganizationService } from 'src/app/services/organization.service';

@Component({
  selector: 'app-api-key',
  templateUrl: './api-key.component.html',
  styleUrls: ['./api-key.component.css']
})
export class ApiKeyComponent implements OnInit {

  apiKeyDTO: any;

  organization: any;

  organizationEmail!: string;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private orgService: OrganizationService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.apiKeyDTO = this.formBuilder.group({
      email: [''],
      password: [''],
      devName: [''],
      devContact: ['']
    });
  }

  applyAPIKey() {
    const user = this.authService.parseJWTWithoutArg();

    this.apiKeyDTO.value.devName = user.sub;

    this.apiKeyDTO.value.devContact = user.phone;

    console.log(this.apiKeyDTO.value);

    this.orgService.requestAPIKey(this.apiKeyDTO.value).subscribe(
      (res) => {
        this.organization = res;
        this.router.navigate(['manage', 'organizations', this.organizationEmail, 'manage', 'orders']);
      },
      (err) => {
      }
    )
  }

  ngOnInit(): void {
    this.organizationEmail = this.route.snapshot.paramMap.get("id")+"";

    this.apiKeyDTO = this.formBuilder.group({
      email: [this.organizationEmail],
      password: [''],
      devName: [''],
      devContact: ['']
    });
  }

}
