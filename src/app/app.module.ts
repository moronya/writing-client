import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './entities/nav/nav.component';
import { LoginComponent } from './auth/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtInterceptorService } from './services/jwt-interceptor.service';
import { NgDynamicBreadcrumbModule } from 'ng-dynamic-breadcrumb';
import { OrganizationsComponent } from './organizations/organizations.component';
import { OrdersComponent } from './orders/orders.component';
import { OrganizationDetailComponent } from './organization-detail/organization-detail.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { RegisterComponent } from './register/register.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { FooterComponent } from './entities/footer/footer.component';
import { HomeComponent } from './entities/home/home.component';

import { MatDatepickerModule } from '@angular/material/datepicker';

import { MatNativeDateModule } from '@angular/material/core';

import { MatFormFieldModule } from '@angular/material/form-field';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ApiKeyComponent } from './entities/api-key/api-key.component';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoginComponent,
    OrganizationsComponent,
    OrdersComponent,
    OrganizationDetailComponent,
    InvoiceComponent,
    RegisterComponent,
    FooterComponent,
    HomeComponent,
    ApiKeyComponent,
  
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgDynamicBreadcrumbModule,
    SweetAlert2Module.forRoot(),
    MatDatepickerModule,

    MatNativeDateModule,

    MatFormFieldModule,

    NoopAnimationsModule,

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
