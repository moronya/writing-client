import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { OrganizationService } from '../services/organization.service';
import { SeasonService } from '../services/season.service';

@Component({
  selector: 'app-organization-detail',
  templateUrl: './organization-detail.component.html',
  styleUrls: ['./organization-detail.component.css']
})
export class OrganizationDetailComponent implements OnInit {

  organization: any;

  userEmail;

  email!: string;

  name!: string;

  phoneNumber!: string;

  @ViewChild('closeWriterModal') closeWriterModal:any;
  @ViewChild('closeManagerModal') closeManagerModal:any;
  @ViewChild('endSeasonSwal') endSeasonSwal:any;
  @ViewChild('startSeasonSwal') startSeasonSwal:any;
  @ViewChild('addWriterSwal') addWriterSwal:any;
  @ViewChild('addManagerSwal') addManagerSwal:any;
  @ViewChild('expelWriterSwal') expelWriterSwal:any;
  
  data:any = {
    organisationId: '',
    startDate: '',
    endDate: '',
    seasonStatus: '',
    createdBy: '',
    updatedOn: ''
  }; 

  endSeasonData:any = {
    id : '',
    createdBy : ''
  }

  addWriterDTO:any = {
    organisationId:'',
    email:''
  }

  addManagerDTO:any = {
    organisationId:'',
    email:''
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private org: OrganizationService,
    private season: SeasonService,
    private auth:AuthService,
    private router : Router
  ) { 
    let userData = this.auth.parseJwt(localStorage.getItem('token') + "");

    this.userEmail = userData.sub;
  }

  /**
   * Start a season
   * @param orgId 
   */
  startSeason(orgId:any){

    this.data.organisationId = orgId; 

    this.data.createdBy = this.userEmail;

    this.data.seasonStatus = 'PROGRESS';

    this.season.startSeason(this.data).subscribe(
      (res)=>{
        this.organization.currentSeasonId = res.id;
        this.startSeasonSwal.fire();
      },
      (err)=>{
      }
    );
  }

  /**
   * End a season, another one is created
   * @param id 
   */
  endSeason(id:number){

    this.endSeasonData.id = id;

    this.endSeasonData.createdBy = this.userEmail;

    this.season.endSeason(this.endSeasonData).subscribe(
      (res)=>{
        this.organization.currentSeasonId = res.id;
        localStorage.setItem("season",res.id);
        this.endSeasonSwal.fire();
      },
      (err)=>{
        console.log(err);
      }
    )
  }


  /**
   * Add a writer to an organization
   */
  addWriter(){

    const data = {
      organisationId:this.organization.id,
      email: this.email,
      name:this.name,
      phoneNumber:this.phoneNumber
      
    }

    this.org.addWriter(data).subscribe(
      (res)=>{
        this.closeWriterModal.nativeElement.click();
        this.organization = res;
        this.addWriterSwal.fire();
      },
      (err)=>{
        console.log(err);
      }
    )

  }


  /**
   * Add a manager to an organization
   */
  addManager(){

    this.addManagerDTO.organisationId = this.organization.id;

    this.addManagerDTO.email = this.email;

    this.org.addManager(this.addManagerDTO).subscribe(
      (res)=>{
        this.closeManagerModal.nativeElement.click();
        this.organization = res;
        this.addManagerSwal.fire();
      },
      (err)=>{

      }
    );
  }


  /**
   * Expel a writer from an organization
   * @param email 
   */
  expelWriter(email:string){

    const data : any = {
      organisationId : this.organization.id,
      email: email
    }

    this.org.expelWriter(data).subscribe(
      (res)=>{
        this.organization = res;
        this.expelWriterSwal.fire();
      },
      (err)=>{
        console.log(err);
      }
    )

  }

  /**
   * Navigate to orders
   */
  toOrders(){
    this.router.navigate(['manage/orders'],{relativeTo:this.activatedRoute});

    localStorage.setItem("writers",JSON.stringify(this.organization.writers));

    localStorage.setItem("season",this.organization.currentSeasonId);
  }

  ngOnInit(): void {
    this.org.getByOrganizationEmail(this.activatedRoute.snapshot.paramMap.get('id') + "").subscribe(
      (res) => {
        this.organization = res;
      },
      (err) => {
      }
    );


  }

}
