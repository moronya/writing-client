import { Component, OnInit, ViewChild } from '@angular/core';

import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../services/order.service';
import { OrganizationService } from '../services/organization.service';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})


export class OrdersComponent implements OnInit {



  searchData!: string;

  searchDataFieldTouched: boolean = false;

  matchedOrders!: Array<any>;

  orderData: any;

  orders: any = [];

  writers: any;

  organization:any;

  @ViewChild('closeOrderModal') closeOrderModal: any;
  @ViewChild('createOrderSwal') createOrderSwal: any;

  constructor(
    private formBuilder: FormBuilder,
    private orderService: OrderService,
   private org: OrganizationService,
   private activatedRoute:ActivatedRoute
  ) {
    this.orderData = this.formBuilder.group({
      orderAmount: [''],
      orderTopic: [''],
      wordCount: [''],
      assigneeEmail: [''],
      orderInstructions: [''],
      orderNumber: [''],
      dueDateTime: [''],
      seasonId: localStorage.getItem("season"),
      organizationId: localStorage.getItem("orgId")
    });
  }



  /*
    Filter orders by orderId
    Input : searchData
    Output : List of orders whose order number matches the searchData
  */
  searchByOrderId() {

    this.matchedOrders = this.orders.filter((order: any) => {
      return order.orderNumber.match(this.searchData);
    });
  }

  /**
   * create an order 
   * input -> formData as orderData.value
   * output -> a created order
   */
  createOrder() {

    this.orderService.createOrder(this.orderData.value).subscribe(
      (res) => {
        this.closeOrderModal.nativeElement.click();
        this.orders.push(res);
        this.createOrderSwal.fire();
        this.orderData.reset();
      },
      (err) => {
        console.log(err);
      }
    )

  }

  minDate = "";
  getCurrentDate() {

    var date = new Date().toISOString().substring(0,16);
    this.minDate = date;  
    console.log("Date: ", this.minDate);
    

  }


  /**
   * calculate the time difference between when the order should be completed and now
   * @param dueDateTime 
   * @returns 
   */
  resolveTimeToDueTime(dueDateTime: Date): string {

    // get the difference in ms (based on epoch time -> 01/01/1970)
    let timeDifferenceInMilliSeconds = new Date(dueDateTime).getTime() - new Date().getTime();

    if (timeDifferenceInMilliSeconds > 0) {
      return this.setHumanReadableTime(timeDifferenceInMilliSeconds) + " remaining";
    } else {
      return this.setHumanReadableTime(timeDifferenceInMilliSeconds) + " due";
    }
  }

  /**
   * create a human readable time difference
   * @param timeDifferenceInSeconds 
   * @returns 
   */
  setHumanReadableTime(timeDifferenceInSeconds: number): string {

    let absoluteValue = Math.abs(timeDifferenceInSeconds);

    if (absoluteValue <= 3600000) {
      return this.checkMinutes(absoluteValue);
    } else if (absoluteValue > 3600000 && absoluteValue <= 86400000) {
      return this.checkHours(absoluteValue);
    } else {
      return this.checkDays(absoluteValue);
    }
  }

  /**
   * Check the minutes time difference (<60 minutes)
   * @param absoluteTimeDifference 
   * @returns 
   */
  checkMinutes(absoluteTimeDifference: number): string {

    let minutesCount = Math.floor(absoluteTimeDifference / 60000);

    if (minutesCount > 1 || minutesCount == 0) {
      return minutesCount + " minutes";
    } else {
      return minutesCount + "minute";
    }
  }

  /**
   * Check the hour difference -> X hours and Y minutes
   * @param absoluteTimeDifference 
   * @returns 
   */
  checkHours(absoluteTimeDifference: number): string {
    let hrs = Math.floor(absoluteTimeDifference / 60000 / 60);

    if (hrs % 24 == 0) {
      return hrs / 24 + "days";
    }

    let minutes = Math.floor(((absoluteTimeDifference / 3600000) - hrs) * 60);

    if (minutes > 1 || minutes == 0) {
      return hrs + " hours " + minutes + " minutes";
    } else {
      return hrs + " hours " + minutes + " minute";
    }
  }

  /**
   * Check the days time difference -> X days and Y hours
   * @param absoluteTimeDifference 
   * @returns 
   */
  checkDays(absoluteTimeDifference: number): string {
    let days = Math.floor(absoluteTimeDifference / 60000 / 60 / 24);

    let hrs = Math.floor(((absoluteTimeDifference / 86400000) - days) * 24);

    let daysRepresentation;

    let hoursRepresentation;

    if (days > 1 || days == 0) {
      daysRepresentation = days + " days";
    } else {
      daysRepresentation = days + " day";
    }

    if (hrs > 1 || hrs == 0) {
      hoursRepresentation = hrs + " hours";
    } else {
      hoursRepresentation = hrs + " hours";
    }


    return daysRepresentation + " " + hoursRepresentation;

  }


  /**
   * Approve an order
   * @param order 
   */
  approveOrder(order: any) {

    const data: any = {
      currentSeasonId: localStorage.getItem('season'),
      order: order
    }
    // call the service to approve
    this.orderService.approveOrder(data).subscribe(
      (res) => {

        let orderNumber = res.orderNumber;

        this.setNewOrderState(orderNumber, "COMPLETED");

      },
      (err) => {
        console.log(err);
      }
    )
  }

  /**
   * Cancel an order
   * @param order 
   */
  cancelOrder(order: any) {
    this.orderService.cancelOrder(order).subscribe(
      (res) => {
        let orderNumber = res.orderNumber;

        this.setNewOrderState(orderNumber, "CANCELLED");
      },
      (err) => {
        console.log(err);
      }
    )
  }

  /**
   * Change order state
   * @param orderNumber 
   * @param newState 
   */
  setNewOrderState(orderNumber: string, newState: string) {
    let currentOrder = this.orders.find((order: any) => {
      return order.orderNumber === orderNumber;
    });

    currentOrder.orderStatus = newState;
  }

  ngOnInit(): void {

  this.getCurrentDate();
  this.getOrganization();

    const data = {
      organizationId: localStorage.getItem('orgId'),
      seasonId: localStorage.getItem('season')
    }

    this.orderService.getOrders(data).subscribe(
      (res) => {
        this.orders = res;
      },
      (err) => {
        console.log(err);
      }
    );

    this.writers = JSON.parse(localStorage.getItem('writers') || '{}');

  }

  //retrieve organization

  getOrganization(){
    var currentOrg = this.org.getByOrganizationEmail(
      this.activatedRoute.snapshot.paramMap.get('id') + "").subscribe(
        (res) => {
          this.organization = res;
        },
        (err) => {
        }
      );
      console.log("Current organization: ", currentOrg);

  }



}
