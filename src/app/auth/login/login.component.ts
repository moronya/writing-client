import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { OrganizationService } from 'src/app/services/organization.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData: any;

  userData: any;

  isLoggedIn!: boolean;

  message = '';

  loginFail = false;

  showPassword = false;

  defaultOrg: any;


  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private org: OrganizationService
  ) {
    this.loginData = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }

  loginUser() {

    /*
      Call auth service
      Input -> LoginData
      Processs -> save token to localstorage, decode jwt token, route
      Output -> success/failure
    */
    this.auth.login(this.loginData.value).subscribe(
      (res) => {

        this.saveTokenToStorage(res.idToken);

        this.userData = this.auth.parseJwt(res.idToken);
        console.log("User data", this.userData)
        


        const role = this.userData.auth;

        this.route(role, this.userData.sub);


      },
      (err) => {
        console.log(err.message);
        this.loginFail = true;
        this.message = "The username or password entered does not exist";
        this.loginData.reset();
      }
    );

  }

  toggleLoginPassword() {
    const userPassword = document.getElementById('loginPassword') as HTMLInputElement;
    this.showPassword = !this.showPassword;

    if (userPassword.type === 'password') {
      userPassword.type = 'text';
    } else {
      userPassword.type = 'password';
    }

  }


  /*
    input : token
    output : saved token on localstorage : to be sent in every request for auth(entication/orization)
  */
  saveTokenToStorage(token: string) {
    localStorage.setItem("token", token);
  }

  logout() {
    this.isLoggedIn = false;
    this.auth.clearToken();
  }


  /* 
    routing user
    input : auth role
    output : role based routing
  */
  route(role: string, email?: any) {
    if (role == "MANAGER") {
      this.org.getByManagerEmail(email).subscribe(
        (res) => {
          const org = res[0];

          // save all organizations to local storage
          this.org.saveOrgsToStorage(res);

          // route to manager page
          this.router.navigate(['manage', 'organizations', org?.email, 'manage', 'orders']);
        },
        (err) => {
          console.log(err);
        }
      );
    } else if (role == "WRITER") {
    } else {
    }
  }


  ngOnInit(): void {
    if (localStorage.getItem('token') != null) {
      this.userData = this.auth.parseJwt(localStorage.getItem('token') + '');

      const role = this.userData.auth;

      this.route(role);
    }
  }

}
