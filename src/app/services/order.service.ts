import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * create an order 
   * @param data 
   * @returns 
   */
  createOrder(data: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "order/create", data);
  }

  /**
   * get orders
   */
  getOrders(data:any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "orders",data);
  }

  /**
   * Approve an order -> COMPLETED status
   * @param order 
   * @returns 
   */
  approveOrder(order: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "orders/approve", order);
  }

  /**
   * Cancel an order
   * @param order 
   * @returns 
   */
  cancelOrder(order: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "orders/cancel", order);
  }

}
