import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  ORG_KEY = "orgs";

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Create an organization
   * @param data 
   * @returns 
   */
  createOrganization(data: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "organization/register", data);
  }

  /**
   * Get an organization by owner email
   * @param email 
   * @returns 
   */
  getByManagerEmail(email: string): Observable<any> {
    return this.http.get(Constants.API_ENDPOINT + "organization/" + email);
  }

  /**
   * Get an organization by organization email
   * @param email 
   * @returns 
   */
  getByOrganizationEmail(email: string): Observable<any> {
    return this.http.get(Constants.API_ENDPOINT + "organizations/" + email);
  }

  /**
   * Add a writer to an organization
   * @param data 
   * @returns 
   */
  addWriter(data: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "organization/writers/add", data);
  }

  /**
   * Add a manager to an organization
   * @param data 
   * @returns 
   */
  addManager(data: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "organization/managers/add", data);
  }

  /**
   * Expel a writer from an organization
   * @param data 
   * @returns 
   */
  expelWriter(data: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "organization/writers/remove", data);
  }

  /**
   * Request API KEY
   */
  requestAPIKey(data: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "organization/request-apikey", data);
  }

  /**
   * Save organisations to storage
   */
  saveOrgsToStorage(data: any){
    localStorage.setItem(this.ORG_KEY, data);
  }
}
