import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Constants } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  JWT_KEY = 'token'

  /*
    Login
    Input - email/password
    Output - token
  */
  login(data: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "auth", data);
  }

  /*
    Logout
    Delete token from localstorage
  */
    
  public clearToken() {
    localStorage.removeItem(this.JWT_KEY);
  }

  /*
    decode jwt token
    input : jwt token
    output : user object with email and role
  */
  parseJwt(token: string) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  };

  parseJWTWithoutArg(){

    return this.parseJwt(localStorage.getItem(this.JWT_KEY)+"");
  }

  // get the stored token
  public getToken():any{
    return localStorage.getItem(this.JWT_KEY) || sessionStorage.getItem(this.JWT_KEY);
  }

  createUser(data:any){
    return this.http.post(Constants.API_ENDPOINT+"signup",data);
  }

}
