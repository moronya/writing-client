import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Get invoices by organization id
   * @param id 
   * @returns 
   */
  getInvoice(id: string): Observable<any> {
    return this.http.get(Constants.API_ENDPOINT + "invoice/" + id)
  }
}
