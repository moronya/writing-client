import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class SeasonService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Start a season
   * @param data 
   * @returns 
   */
  startSeason(data: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "season", data);
  }

  /**
   * End a season
   * @param data 
   * @returns 
   */
  endSeason(data: any): Observable<any> {
    return this.http.post(Constants.API_ENDPOINT + "season/end", data);
  }
}
