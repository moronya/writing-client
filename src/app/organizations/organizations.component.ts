import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { OrganizationService } from '../services/organization.service';

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.css']
})
export class OrganizationsComponent implements OnInit {

  orgData: any;

  managers = [];

  organizations: any;

  userEmail: string;

  @ViewChild('closeCreateOrgModal') closeOrgModal: any;
  @ViewChild('createOrganizationSwal') createOrganizationSwal:any;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private org: OrganizationService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {

    let userData = this.auth.parseJwt(localStorage.getItem('token') + "");

    this.userEmail = userData.sub;

    this.orgData = this.formBuilder.group({
      email: ['', Validators.required],
      status: [],
      statusReason: [''],
      currentSeasonId: [''],
      managerEmail: [userData.sub],
      managers: []
    });
  }

  /**
   * Create an organization
   */
  createOrg() {
    this.org.createOrganization(this.orgData.value).subscribe(
      (res) => {
        this.organizations.push(res);
        this.orgData.reset();
        this.closeOrgModal.nativeElement.click();
        this.createOrganizationSwal.fire();
      },
      (err) => {
        console.log(err);
      }
    )
  }

  toDetail(org:any) {
    localStorage.setItem('orgId', org.id);

    console.log(org.currentSeasonId);
    localStorage.setItem('season',org.currentSeasonId);

    localStorage.setItem('writers',JSON.stringify(org.writers))

    this.router.navigate([org.email], { relativeTo: this.activatedRoute });
  }

  ngOnInit(): void {

    this.org.getByManagerEmail(this.userEmail).subscribe(
      (res) => {
        this.organizations = res;
      },
      (err) => {
        console.log(err);
      }
    )
  }

}
