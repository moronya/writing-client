import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InvoiceService } from '../services/invoice.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  invoices: any;

  currentInvoice: any;

  activeId!: number;

  previousId!: number;

  nextId!: number;

  searchedEmail!: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private invoiceService: InvoiceService
  ) { }

  /**
   * Move to next order
   */
  next() {

    if (this.activeId == this.invoices.length - 1) {
      this.activeId = this.invoices.length - 1;
    } else {
      this.activeId = this.activeId + 1;
    }

    this.currentInvoice = this.invoices[this.activeId];
  }


  /**
   * Move to previous order
   */
  previous() {

    if (this.activeId == 0) {
      this.activeId = 0;
    } else {
      this.activeId = this.activeId - 1;
    }

    this.currentInvoice = this.invoices[this.activeId];
  }

  /**
   * Filter invoices by email
   * Return invoice for email
   */
  filterByEmail() {

    this.currentInvoice = this.invoices.find((invoice: { email: string; }) => {
      return invoice.email == this.searchedEmail;
    });

  }

  /**
   * Export invoice as CSV
   * @param invoice 
   */
  extractCSV(invoice: any) {
    console.log(invoice);
    var json = invoice.orders;
    var fields = Object.keys(json[0])
    var replacer = (key: any, value: any) => { return value === null ? '' : value }
    var csv = json.map(function (row: any) {
      return fields.map(function (fieldName) {
        return JSON.stringify(row[fieldName], replacer)
      }).join(',')
    })
    csv.unshift(fields.join(',')) // add header column
    csv = csv.join('\r\n');

    var downloadLink = document.createElement("a");
    var blob = new Blob(["\ufeff", csv]);
    var url = URL.createObjectURL(blob);
    downloadLink.href = url;
    downloadLink.download = invoice.email + "-" + new Date().toJSON() + ".csv";  //Name the file here
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  }

  ngOnInit(): void {


    this.invoiceService.getInvoice(this.activatedRoute.snapshot.paramMap.get('orgid') + "").subscribe(
      (res) => {
        this.invoices = res;


        for (let invoice of this.invoices) {
          let totalCost = 0;

          for (let order of invoice.orders) {
            if (order.orderStatus == "COMPLETED") {
              totalCost = totalCost + order.orderAmount;
            }

            invoice.totalAmount = totalCost;
          }
        }

        this.activeId = 0;
        this.currentInvoice = this.invoices[this.activeId];

      },
      (err) => {
        console.log(err);
      }
    )
  }

}
