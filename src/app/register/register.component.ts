import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { confirmedValidator } from './confirmedValidator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userData: any;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) {
    this.userData = this.formBuilder.group({
      email: [''],
      phoneNumber: [''],
      password: ['', [Validators.required]], 
      confirmPassword:['', [Validators.required]]
    },{
      validator:confirmedValidator('password', 'confirmPassword')
    }
    )
  }
  get f(){
    return this.userData.controls;
  }

  createUser() {
    console.log(this.userData);

    this.auth.createUser(this.userData.value).subscribe(
      () => {
        this.router.navigate(['/login']);
      },
      () => {
      }
    )
  }


  ngOnInit(): void {
  }

}
